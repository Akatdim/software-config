(load-file "/home/dmitry/.emacs.d/emacs-for-python/epy-init.el")
(add-to-list 'load-path "/home/dmitry/.emacs.d/emacs-for-python/") ;; tell where to load the various files
(add-to-list 'load-path "/home/dmitry/.emacs.d/themes/solarized/")

(desktop-save-mode 1)
(menu-bar-mode -1)
(setq desktop-enable t)
(desktop-load-default)

(require 'color-theme)
(require 'color-theme-solarized)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-solarized-dark)))

(require 'epy-setup)      ;; It will setup other loads, it is required!
 (require 'epy-python)     ;; If you want the python facilities [optional]
 (require 'epy-completion) ;; If you want the autocompletion settings [optional]
 (require 'epy-editing)    ;; For configurations related to editing [optional]
 (require 'epy-bindings)   ;; For my suggested keybindings [optional]
 (require 'epy-nose)       ;; For nose integration
 (setq indent-tabs-mode t
      tab-width 4
      python-indent 4)
(epy-setup-ipython)